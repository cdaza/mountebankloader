const mb = require('mountebank');
const settings = require('./settings');
const mockService = require('./mockService');
const fs = require('fs'); 
const jsonfile = require('jsonfile')

const dir = './stubs';

const mbServerInstance = mb.create({
    port: settings.port,
    pidfile: '../mb.pid',
    logfile: '../mb.log',
    protofile: '../protofile.json',
    ipWhitelist: ['*']
});

mbServerInstance.then(function () {
    fs.readdir(dir,  (err, files) => {
        files.forEach(file => {
            if( file.substring(file.lastIndexOf('.')) === ".json") {
                let configStubFileName = file.substring(0, file.lastIndexOf('.')); 
                jsonfile.readFile(`${dir}/${file}`, function (err, jsonFile) {
                    if (err) {
                        console.error(err)
                    }
                    mockService.addImposter(jsonFile, `${configStubFileName}Config.js`);
                  })
            }
        });
      });
});
