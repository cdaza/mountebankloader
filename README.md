After download the project: move to the folder project

For install dependencies:
    - npm install 
To run:
    - npm start
    
if you want to add new stubs. Use the stubs folder. You have to put 2 files:

1) The stub in .json formmat.
2) The configuration file in .js format

The configuration file must be contain the same name that stub file plus word "Config"

Example : 
stub file : myStubFile.json
conf file : myStubFileConfig.js

