module.exports = {
    predicates: [ {
        equals: {
            method: "GET",
            path: "/"
        }
    }],
    responses: [
        {
            is: {
                statusCode: 200,
                headers: {
                    "Content-Type": "application/json"
                }
            }
        }
    ],
    port: 3043
}