const fetch = require('node-fetch');
const settings = require('./settings');

function postImposter(bodyImposter) {
    const url = `${settings.server}:${settings.port}/imposters`;

    return fetch(url, {
                    method:'POST',
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify(bodyImposter)
                });
}

module.exports = { postImposter };