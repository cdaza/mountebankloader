const mbLoader = require('./mountebank-loader');
const settings = require('./settings');

function addImposter(stubFile, configStubFileName) {

    const configStubFile = require(`./stubs/${configStubFileName}`);
    configStubFile.responses[0].is.body = stubFile;
    const stubs = [
        {
            predicates: configStubFile.predicates,
            responses: configStubFile.responses
        }
    ];

    const imposter = {
        port: configStubFile.port,
        protocol: 'http',
        stubs: stubs
    };

    return mbLoader.postImposter(imposter);
}

module.exports = { addImposter };